mgEveDB Module Installation

A database must be added to the drupal databases array in the site's settings.php file for the Eve database.

Here is the Drupal documentation on configuring multiple databases:
http://drupal.org/node/310071


IMPORTANT!!!
1. The database user this module is configured to use should only have SELECT access to the Eve Database!
2. The Eve Database should be a standalone database with no other data in it

The query module only performs minimal checking on the query strings passed in. If a module is installed that calls it with malicious SQL strings, it could expose data to risk if these two guidelines are not followed.



Example settings.php databases array:

$databases = array (
  'default' => 
  array (
    'default' => 
    array (
      'database' => 'drupalDB',
      'username' => 'drupalDB_user',
      'password' => 'strongpassword',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
  

  'evedump' => 
  array (
    'default' => 
    array (
      'database' => 'eveDB',
      'username' => 'eveDB_user',
      'password' => 'anotherpassword!',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);