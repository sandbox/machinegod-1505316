// $Id$

DEVELOPER DOCUMENTATION

This module provides a wrapper to a database connection for the Eve data dump.
Information about the data dump can be found here:
http://wiki.eve-id.net/CCP_Community_Toolkit

The database needs to be added to the list of databases in the databases array in the settings.php page for the site.


This module essentially wraps a call to db_set_active($key) to set the current db to the Eve db, and then set it back when done.

The eve DB is read only, so this module only exposes a query function for the DB itself


function mgevedb_query($query => null) 



These functions will return the path to the picture file for the specified item

function mgevedb_getcorpicon($id)

function mgevedb_getitemicon($id)    
function mgevedb_gettypeicon($id)

function mgevedb_getrender($id)


