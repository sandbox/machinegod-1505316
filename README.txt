// $Id$

mgEveDB - Eve Online data dump wrapper for the mgEve drupal modules

----------------------------------------------------


Usage Notes
-----------------------
IMPORTANT!!!
1. The database user this module is configured to use should only have SELECT access to the Eve Database!
2. The Eve Database should be a standalone database with no other data in it

The query module only performs minimal checking on the query strings passed in. If a module is installed that calls it with malicious SQL strings, it could expose data to risk if these two guidelines are not followed.


Known incompatibilities
-----------------------

No incompatibilities with other modules are known to this date.


Maintainers
-----------
mgEveDB is written by Michael Lehman - Drakos Wraith in game.


